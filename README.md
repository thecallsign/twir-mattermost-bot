# This Week in Rust Bot

## Post weekly updates about Rust to Mattermost

This bot needs some enviroment variables to work.
You can set them in the enviroment manually or create a `.env` file

The keys needed are:

- URL:        The base url of the mattermost instance (eg. https://mattermost.example.net)
- CHANNEL:    The channel that the bot should post to
- TEAM:       The name of the team that the channel belongs to
- API_KEY:    The api key or auth token for the bot

Optional Keys:

- DATA_PATH:  This bot stores the timestamp of the last sent article on disk. 
            The default file path is `./data`. Use this key to change the path.
- TIMEOUT:    How often should the bot check for updates in seconds. Default is 3600 (1 hour).

Please make an issue if you find something wrong.
