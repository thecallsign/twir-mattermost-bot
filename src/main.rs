#[macro_use]
extern crate serde;

mod error;
mod mattermost;

use std::fs::{read_to_string, write, File};
use std::path::PathBuf;

use std::thread::sleep;
use std::env::var as get_env;

use std::result::Result as StdResult;


use atom_syndication::{Entry, Feed};
use chrono::prelude::*;
use reqwest::Client;
use dotenv::dotenv;

use error::Error;
use mattermost::*;

/// Our own Result object
type Result<T> = StdResult<T, Error>;
type Timeout = i64;

const TWIR_ATOM_FEED_URL: &str = "https://this-week-in-rust.org/atom.xml";
const DEFAULT_DATA_PATH: &str = "data";

#[cfg(not(debug_assertions))]
const DEFAULT_TIMEOUT: Timeout = 3600;
#[cfg(debug_assertions)]
const DEFAULT_TIMEOUT: Timeout = 5;

#[derive(Debug)]
enum State {
    Init,
    ShouldPost(Entry),
    CheckForUpdate,
    UpToDate,
    Timeout(Timeout),
    TimeoutExpired
}

struct StateContainer(State);
impl StateContainer {
    pub fn switch(&mut self, new_state: State) {
        #[cfg(debug_assertions)]
        {
            match (&self.0, &new_state) {
                // The Entry object is really big and pollutes our debug output, lets show a summary instead
                (_, State::ShouldPost(_)) => eprintln!("[STATE CHANGE] {:?} ====> ShouldPost(Entry {{...}})", &self.0),
                (State::ShouldPost(_), _) => eprintln!("[STATE CHANGE] ShouldPost(Entry {{...}}) ====> {:?}", &new_state),
                _ => eprintln!("[STATE CHANGE] {:?} ====> {:?}", &self.0, &new_state)
            };
        }
        self.0 = new_state;
    }

    pub fn inner(&self) -> &State {
        &self.0
    }
}



fn main() -> StdResult<(), Box<dyn std::error::Error>> {
    dotenv().ok();
    let client = Client::new();
    let url = get_env("URL")
        .expect(&missing_key_msg("URL"));
    let channel = get_env("CHANNEL")
        .expect(&missing_key_msg("CHANNEL"));
    let api_key = get_env("API_KEY")
        .expect(&missing_key_msg("API_KEY"));
    let team = get_env("TEAM")
        .expect(&missing_key_msg("TEAM"));
    
    let data_path = get_env("DATA_PATH")
        .map_err(|_| {
            eprintln!("DATA_PATH env var not present, using default {}.", DEFAULT_DATA_PATH);
            std::env::VarError::NotPresent
        })
        .unwrap_or_else(|_| DEFAULT_DATA_PATH.to_owned());
    let data_path = PathBuf::from(data_path);

    let timeout = get_env("TIMEOUT")
        .and_then(|x| x.parse::<i64>().map_err(|_| {
            eprintln!("TIMEOUT env var not present, using default {}.", DEFAULT_TIMEOUT);
            std::env::VarError::NotPresent
        }))
        .unwrap_or_else(|_| DEFAULT_TIMEOUT);

    let api_config = APIConfig {
        url,
        api_key,
        team,
        channel
    };

    let mut app = Application::new(client, timeout, api_config, data_path);

    loop {
        app.run()?;
    }
}

fn missing_key_msg(key: &str) -> String {
    format!("NO {key} SET\nEither set an env var or place it in a file alongside this binary\nThe key missing is: {key}", key=key)
}


struct Application {
    state: StateContainer,
    data_path: PathBuf,
    timeout: Timeout,
    api: MattermostAPI
}


impl Application {
    pub fn new(client: Client, timeout: Timeout, api_config: APIConfig, data_path: PathBuf) -> Application {
        let api = MattermostAPI::new(client, api_config);
        let app = Application {
            api,
            state: StateContainer(State::Init),
            data_path,
            timeout
        };
        app
    }

    fn fetch_content(&self) -> Result<String> {
        let mut resp = self.api.client().get(TWIR_ATOM_FEED_URL).send()?;
        resp.text().map_err(|err| err.into())
    }

    fn parse_context(&self, bytes: &[u8]) -> Result<Feed> {
        let feed = Feed::read_from(bytes);
        feed.map_err(|err| err.into())
    }

    fn check_last_update(&self, feed: &Feed) -> Result<State> {
        
        let last_update = match read_to_string(&self.data_path) {
            Ok(text) => DateTime::parse_from_rfc3339(&text.trim_end()).unwrap_or_else(|_| {
                return DateTime::from_utc(chrono::NaiveDateTime::from_timestamp(0, 0), chrono::FixedOffset::east(0))
            }),
            Err(_) => {
                File::create(&self.data_path)?;
                return Ok(State::CheckForUpdate);
            }
        };

        let new_entry: Entry = match feed.entries().into_iter().cloned().next() {
            Some(s) => s,
            None => {
                eprintln!("No entry list in fetched feed! Something is wrong!");
                return Err(Error::FeedError)
            }
        };
        let new_entry_last_updated = DateTime::parse_from_rfc3339(new_entry.updated())?;
        if new_entry_last_updated.signed_duration_since(last_update).num_seconds() > 0 {
            write(&self.data_path, new_entry_last_updated.to_rfc3339())?;
            Ok(State::ShouldPost(new_entry))
        } else {
            Ok(State::UpToDate)
        }
    }

    // Handle the current state.
    fn run(&mut self) -> Result<()> {
        let state = &self.state.inner();
        let new_state = match state {
            State::ShouldPost(e) => {
                self.post(e)?;
                State::UpToDate
            },
            State::Init => {
                State::CheckForUpdate
            },
            State::UpToDate => {
                State::Timeout(self.timeout)
            },
            State::Timeout(timeout) => {
                let dur = std::time::Duration::from_secs(*timeout as u64);
                sleep(dur);
                State::TimeoutExpired
            },
            State::TimeoutExpired => {
                State::CheckForUpdate
            }
            State::CheckForUpdate => {
                let feed = self.get_feed()?;
                let new_state = self.check_last_update(&feed)?;
                new_state
            }
        };
        self.state.switch(new_state);
        Ok(())
    }

    /// Post an article to the channel
    fn post(&self, entry: &Entry) -> Result<()>{
        let mut post_str = String::new();
        let link = entry.links()
            .first()
            .and_then(|l| Some(l.href()))
            .or_else(|| Some(""))
            .unwrap();
        post_str.push_str(entry.title());
        post_str.push_str("\n");
        post_str.push_str(link);
        let res = self.api.post_message(post_str)?;
        if !res.ok() {
            let mut response = res.inner();
            let body = response.text();
            eprintln!("[ERROR] Got response from server {:?}\n{:?}", response, body);
        }
        Ok(())
    }

    fn get_feed(&self) -> Result<Feed> {
        let data = self.fetch_content()?;
        self.parse_context(data.as_bytes())
    }
}
