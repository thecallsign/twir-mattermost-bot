use std::error::Error as StdError;
use std::fmt::Display;

type RequestError = reqwest::Error;
type AtomError = atom_syndication::Error;
type DateTimeError = chrono::ParseError;
type FileError = std::io::Error;

#[derive(Debug)]
pub enum Error {
    FeedError,
    FileError(FileError),
    ReqwestError(RequestError),
    AtomError(AtomError),
    DateTimeError(DateTimeError)
}

impl From<RequestError> for Error {
    fn from(error: RequestError) -> Error {
        Error::ReqwestError(error)
    }
}

impl From<AtomError> for Error {
    fn from(error: AtomError) -> Error {
        Error::AtomError(error)
    }
}

impl From<DateTimeError> for Error {
    fn from(error: DateTimeError) -> Error {
        Error::DateTimeError(error)
    }
}

impl From<FileError> for Error {
    fn from(error: FileError) -> Error {
        Error::FileError(error)
    }
}

impl Display for Error {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Error::FeedError => {
                fmt.write_str("Network Error")
            },
            Error::ReqwestError(e) => {
                e.fmt(fmt)
            },
            Error::FileError(e) => {
                e.fmt(fmt)
            },
            Error::AtomError(e) => {
                fmt.write_str(&format!("{:?}", e))
            },
            Error::DateTimeError(e) => {
                e.fmt(fmt)
            }
        }
    }
}

impl StdError for Error {}
