//! Handle everything mattermost related

use reqwest::{Response, Client};

#[derive(Debug)]
pub struct APIConfig {
    pub url: String,
    pub api_key: String,
    pub team: String,
    pub channel: String,
}

#[derive(Debug)]
pub struct MattermostAPI {
    api_key: String,
    base_url: String,
    chan_id: String,
    team_name: String,
    chan_name: String,
    client: Client
}

/// A struct to deserialize a mattermost reponse into. 
/// We only care about the id field so we leave out everything else.
#[derive(Deserialize)]
struct ChanIDResponse {
    pub id: String
}

pub struct MattermostPostResponse {
    response: Response
}

impl MattermostPostResponse {
    pub fn ok(&self) -> bool {
        let code = self.response.status().as_u16();
        match code {
            200 | 201 => true,
            _ => false
        }
    }

    pub fn inner(self) -> Response {
        self.response
    }
}
impl MattermostAPI {
    pub fn new(client: Client, config: APIConfig) -> MattermostAPI {
        let (uri, api_key, team_name, chan_name) = (config.url, config.api_key, config.team, config.channel);
        let mut res = client
            .get(&format!("{uri}/api/v4/teams/name/{team}/channels/name/{chan}", uri=uri, team=team_name, chan=chan_name))
            .bearer_auth(&api_key)
            .send()
            .map_err(|x| eprintln!("{}", x))
            .expect("Failed to init mattermost api");

        match res.json::<ChanIDResponse>() {
            Ok(s) => {
                MattermostAPI {
                    base_url: uri,
                    api_key: api_key,
                    chan_id: s.id,
                    team_name,
                    chan_name,
                    client
                }
            },
            _ => panic!("Unable to init api client")
        }
    }
    
    pub fn post_message(&self, payload: String) -> Result<MattermostPostResponse, reqwest::Error> {
        let req = MattermostRequest {
            message: payload,
            channel_id: self.chan_id.clone()
        };

        let res = self.client.post(&format!("{}/api/v4/posts", &self.base_url))
            .bearer_auth(&self.api_key)
            .json(&req).send()?;
        
        Ok(MattermostPostResponse {
            response: res
        })
    }

    /// Get the Reqwest client of this api
    pub fn client(&self) -> &Client {
        &self.client
    }
}

#[derive(Debug, Serialize)]
pub struct MattermostRequest {
    pub message: String,
    pub channel_id: String
}
